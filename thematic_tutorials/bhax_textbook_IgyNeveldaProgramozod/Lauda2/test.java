public class test{
    public static void main(String[] args) throws ParentException {
        //Object input=1F;
        //Object input=null;
        Object input="ASD";
        try{
            System.out.println("Try!");
            if(input instanceof Float){
                throw new ChildException();
            }else if(input instanceof String){
                throw new ParentException();
            }else{
                throw new RuntimeException();
            }
        }catch(ChildException e){
            System.out.println("Child exception is caught!");
            if(e instanceof ParentException){
                throw new ParentException();
            }
        }catch(ParentException e){
            System.out.println("Parent exception is caught!");
            System.exit(1);
        }catch(Exception e){
            System.out.println("Exception is caught!");
        }finally{
            System.out.println("Finally!");
        }

    }
}