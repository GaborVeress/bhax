
import java.util.Random;

public class PolarGenerator { //osztálydefiníció
    boolean nincsTarolt; //adattagok
    double tarolt;
    public PolarGenerator() { //konstruktor: new szónál hívódik meg és ezzel hozunk létre objektumokt
        nincsTarolt = true;
    }
    public double kovetkezo() { //egy függvény (van visszatérési értéke)
        if(nincsTarolt) {
            double u1, u2, v1, v2, w;
            do {
                u1 = Math.random();
                u2 = Math.random();
                v1 = 2*u1 - 1;
                v2 = 2*u2 - 1;
                w = v1*v1 + v2*v2;
            } while(w > 1);
            double r = Math.sqrt((-2*Math.log(w))/w);
            tarolt = r*v2;
            nincsTarolt = false;
            return r*v1;
        } else {
            nincsTarolt = true;
            return tarolt;
        }
    }
    public static void main(String[] args) {
        PolarGenerator pg = new PolarGenerator(); //Létre hozunk egy példányt (objektumot)
        for(int i = 0; i < 10; i++) {
            System.out.println(pg.kovetkezo());
        }
    }
}