#include <iostream>
int * intPtrFunc(int a)
{
	return  &a;
}
typedef  int (belsoMetodus)(int a, int b);
typedef belsoMetodus * (fgvPtrVisszaadoFgv)(int a);//egészet visszaadó és két egészet váró függvényre mutató mutatót visszaadó, egészet kapó függvény
int main()
{
	int a=10; //egész
	int * int_ptr; //egészre mutató pointer
	int & int_ref=a; //egész referenciája
	int tomb[4]; //egészek tömbje
	int & tomb_ref=tomb[0]; //tömb referencia
	int * ptr_tomb[4]={&tomb[0],&tomb[1],&tomb[2],&tomb[3]}; //egészre mutató mutatók tömbje
	int * (*func_ptr)(int) = &intPtrFunc;//egészre mutató mutatót visszaadó függvényre mutató mutató
	//belsoMetodus (*func_ptr_2)(int)=&fgvPtrVisszaadoFgv; //Függvénymutató egy egészet visszaadó és két egész paraméterű függvényre mutató mutatót visszadó, egészet kapó függvény
	//belsoMetodus * (func_ptr_2)=fgvPtrVisszaadoFgv; //Függvénymutató egy egészet visszaadó és két egész paraméterű függvényre mutató mutatót visszadó, egészet kapó függvény
	//fgvPtrVisszaadoFgv * methodPointer = fgvPtrVisszaadoFgv(belsoMetodus *); //Függvénymutató egy egészet visszaadó és két egész paraméterű függvényre mutató mutatót visszadó, egészet kapó függvény
	void (*fgvPtrVisszaadoFgv)(int);
std::cout<<"Sikeres lefutás!\n";
}
