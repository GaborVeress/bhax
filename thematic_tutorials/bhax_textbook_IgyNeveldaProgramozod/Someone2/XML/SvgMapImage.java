import java.awt.Rectangle;
import java.awt.Graphics2D;
import java.awt.Color;
import java.io.Writer;
import java.io.OutputStreamWriter;
import java.io.IOException;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import java.awt.*;
import java.io.Writer;
import java.awt.Graphics2D;

public class SvgMapImage implements MapImage {

    SVGGraphics2D svgGenerator;
    szin szinek = new szin();

    public SvgMapImage(int canvasWidth, int canvasHeight) {
        // Get a DOMImplementation.
        DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();
        // Create an instance of org.w3c.dom.Document.
        String svgNS = "http://www.w3.org/2000/svg";
        Document document = domImpl.createDocument(svgNS, "svg", null);
        // Create an instance of the SVG Generator.
        svgGenerator = new SVGGraphics2D(document);
        svgGenerator.setSVGCanvasSize(new Dimension(canvasWidth, canvasHeight));
    }

    @Override
    public void addPoint(double x, double y, String state) {//ez a MapImage.javaban és a City.javaban van meghívva
        //hozzáadjuk: ebbe levan kezelve a duplikáció ha már esetleg létezne.
    	szinek.add(state);
    	//kerese meg a hozzátartozó színt
    	int i = 0;
    	while (!szinek.state.get(i).equals(state))
    		i++;
    	//állítsa be az "ecset színét" a megfelelőre
    	svgGenerator.setPaint(szinek.color.get(i));
    	//Rajzolja ki a kört
        svgGenerator.fillOval((int) x, (int) y, 2, 2);
    }

    @Override
    public void save(Writer targetStream) {
        try {
            svgGenerator.stream(targetStream);
        } catch (Exception e) {
            throw new RuntimeException("Failed to write image", e);
        }
    }
}
