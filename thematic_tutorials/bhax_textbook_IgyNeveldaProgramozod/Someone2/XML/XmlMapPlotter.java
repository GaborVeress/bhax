import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.awt.Graphics2D;

public class XmlMapPlotter {

    //1. bemeneti paraméter az xml forrása, a 2. az output
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {
        XmlReader reader = new SaxXmlReader(new FileInputStream(args[0]));
        List<City> cities = reader.getCities();
        MapImage image = new SvgMapImage(800, 600);
        cities.forEach(city -> city.plot(image));
        image.save(new FileWriter(args[1]));
    }
}
