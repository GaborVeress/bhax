;; függvény létrehozása
(defun fakt (szam)
;; ha a szám 0 akkor az eredmény 0
    (if (= szam 0)
        1
;; ha nem 0 akkor számolunk
        (* szam (fakt (- szam 1)))
    )
)
;;függvény használata
; (fakt 5)
;;kiírás
(format t "Eredmeny: ")
(format t "~D~%" (fakt 5))
