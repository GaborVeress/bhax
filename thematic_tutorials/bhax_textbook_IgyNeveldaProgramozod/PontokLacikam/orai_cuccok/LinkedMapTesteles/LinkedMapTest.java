package LinkedMap;

import org.hamcrest.MatcherAssert;
import org.junit.Test;

import javax.swing.text.html.Option;
import javax.swing.text.html.parser.Entity;
import java.security.KeyStore;
import java.util.*;

public class LinkedMapTest {
    public MatcherAssert matcher;

    public static final  String MAP_KEY="KULCS";
    public static final  String MAP_KEY2="KULCS2";
    public static final int MAP_VALUE=10;
    public LinkedMap<String, Integer> underTest;

    @Test
    public void sizeGiveBackOneWhenTheMapContainsOneElement()
    {
        //Várt érték
        int expectedValue=1;

        //Given
        underTest.put(MAP_KEY, MAP_VALUE);

        //When
        int actualValue=underTest.size();


        //Then
        //assertThat(expectedValue,actualValue);
    }

    @Test
    public void isEmptyReturnsTrueIfMapIsempty()
    {
        //Given
        boolean expectedValue= true;

        //When
        boolean actualValue=underTest.isEmpty();


        //Then
        //assertThat(expectedValue,actualValue);


    }

    @Test
    public void containsKeyReturnsTrueWhenMapContainsTheKey()
    {
        //Given
        boolean expectedValue= true;
        underTest.put(MAP_KEY,MAP_VALUE);

        //When
        boolean actualValue = underTest.containsKey(MAP_KEY);

        //Then
        //assertThat(expectedValue,actualValue);


    }

    @Test
    public void containsValueReturnsFalseWhenMapNOTContainsTheKey()
    {
        //Given
        boolean expectedValue= false;
        underTest.put(MAP_KEY,MAP_VALUE);


        //When
        boolean actualValue = underTest.containsValue(MAP_VALUE);


        //Then
        //assertThat(expectedValue,actualValue);


    }

    @Test
    public void getReturnsTheValueWhichAssociatedWithTheKey()
    {
        //Given
        int expectedValue= MAP_VALUE;
        underTest.put(MAP_KEY,MAP_VALUE);

        //When
        int actualValue= underTest.get(MAP_KEY);

        //Then
        //assertThat(expectedValue,actualValue);


    }

    @Test
    public void putReturnsWithValueOfThePreviouslyAddedElement()
    {
        //Given
        int expectedValue= MAP_VALUE;

        //When
        int actualValue= underTest.put(MAP_KEY,MAP_VALUE);

        //Then
        //assertThat(expectedValue,actualValue);


    }

    @Test
    public void removeReturnsWithValueOfTheDeletedElement()
    {
        //Given
        int expectedValue= MAP_VALUE;
        underTest.put(MAP_KEY,MAP_VALUE);

        //When
        int actualValue= underTest.remove(MAP_KEY);

        //Then
        //assertThat(expectedValue,actualValue);


    }

    @Test
    public void putAllWillIncreaseTheSizeOfTheMapByONEForEachElementInTheOtherMap()
    {
        //Given
        int expectedValue= 2;
        LinkedMap<String, Integer> example;
        example.put(MAP_KEY,MAP_VALUE);
        example.put(MAP_KEY2,MAP_VALUE);

        //When
        underTest.putAll(example);
        int actaulValue= underTest.size();

        //Then
        //assertThat(expectedValue,actualValue);


    }

    @Test
    public void clearWillMakeTheMapEmpty()
    {
        //Given
        boolean expectedValue= true;
        underTest.put(MAP_KEY,MAP_VALUE);

        //When
        underTest.clear();

        //Then
        boolean actualValue= underTest.isEmpty();
        //assertThat(expectedValue,actualValue);



    }

    @Test
    public void keySetReturnsSetOfKeysWithAllKeys()
    {
        //Given
        Set<String> expectedValue = new HashSet<>();
        underTest.put(MAP_KEY,MAP_VALUE);
        expectedValue.add(MAP_KEY);
        underTest.put(MAP_KEY2,MAP_VALUE);
        expectedValue.add(MAP_KEY2);

        //When
        Set<String> actualValue = underTest.keySet();


        //Then
        //assertThat(expectedValue,actualValue);


    }

    @Test
    public void valuesReturnsACollectionOfAllExistingValuseInTheMApEvenDuplicatedValues()
    {
        //Given
        Collection<Integer> expectedValue= new LinkedList<>();
        underTest.put(MAP_KEY,MAP_VALUE);
        expectedValue.add(MAP_VALUE);
        underTest.put(MAP_KEY2,MAP_VALUE);
        expectedValue.add(MAP_VALUE);

        //When
        Collection<Integer> actualValue= underTest.values();

        //Then
        //assertThat(expectedValue,actualValue);


    }

    @Test
    public void entrySetReturnSetWithTheAllKeyValuePair()           //MIFOLYIK ITT?!?!?!
    {
        //Given
        Set<Map.Entry<String, Integer>> expectedValue = new HashSet<>();
        underTest.put(MAP_KEY,MAP_VALUE);
        expectedValue.add(MAP_KEY,MAP_VALUE);       //ezt így miért nem fogadja el?
        entry -> expectedValue.add(MAP_KEY, MAP_VALUE); //az entry osztály add metódusát hívnám meg a Set osztályra ahogy látom

        underTest.put(MAP_KEY2,MAP_VALUE);
        expectedValue.add(new Map.Entry(MAP_KEY2,MAP_VALUE));   //De ha így csinálom akkor meg absztact osztályt példányosítanék...

        //When
        Set<Map.Entry<String, Integer>> actualValue= underTest.entrySet();

        //Then
        //assertThat(expectedValue,actualValue);


    }

}
