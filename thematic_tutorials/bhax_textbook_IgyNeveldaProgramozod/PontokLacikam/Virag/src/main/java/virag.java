import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class virag {

	public static void main(String[] args) throws FileNotFoundException {
		//iris.data-t dolgozom fel
		
		//beolvasom a fájlt
		file file = new file();
		BufferedReader reader = null;
		try {
			reader = file.readFile(args[0]);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("A fájl megnyitása sikertelen!");
		    return;
		}
		
		//kezdjük el az olvasását
		double sepalLength = 0, sepalWidth = 0, petalLength = 0, petalWidth = 0;
		int sepalCount = 0, petalCount = 0;
		try {
			while(reader.ready()) {
				String s = reader.readLine();
				if(!s.isEmpty()) {
					String[] parts = s.split(",");
					sepalLength += Double.parseDouble(parts[0]);
					sepalWidth += Double.parseDouble(parts[1]);
					petalLength += Double.parseDouble(parts[2]);
					petalWidth += Double.parseDouble(parts[3]);
					sepalCount++;
					petalCount++;
					//System.out.println(petalCount);
				}
				

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("A sor olvasása sikertelen!");
			e.printStackTrace();
			return;
		}
		
		PrintStream outputStream = null;
		if (args.length ==2) {
			outputStream = new PrintStream(new BufferedOutputStream(new FileOutputStream(args[1])), true);
		} else {
			outputStream = System.out;
		}
/*
		String prefix1 = "short text:";
		String prefix2 = "looooooooooooooong text:";
		String msg = "indented";

		 * The second string begins after 40 characters. The dash means that the
		 * first string is left-justified.
		 */
		String formatFejlec= "%-20s %7s\n";
		String format = "%-20s %5f\n";
		//String format = "%-40s%s%n";
		/*
		System.out.printf(format, prefix1, msg);
		System.out.printf(format, prefix2, msg);
*/
		//String seapalLength = "sepalLength: ";
		double s1 = sepalLength/sepalCount;
		double s2 = sepalWidth/sepalCount;
		double s3 =  petalLength/petalCount;
		double s4 = petalWidth/petalCount;

		outputStream.format(formatFejlec,"típus:", "Átlag:");
		outputStream.format(format,"sepalLength", s1);
		outputStream.format(format,"sepalWidth:", s2);
		outputStream.format(format,"sepalLength:",s3);
		outputStream.format(format,"sepalWidth:", s4);


	}
	
}
