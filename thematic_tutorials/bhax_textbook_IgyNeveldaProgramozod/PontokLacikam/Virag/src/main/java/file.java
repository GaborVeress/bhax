import java.io.BufferedReader;
import java.io.File;  // Import the File class
import java.io.FileNotFoundException;  // Import this class to handle errors
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner; // Import the Scanner class to read text files


public class file {
	
	public BufferedReader readFile(String fileName) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(fileName));
		if (in.ready())
			return in;
		else
			System.out.println("A fájl üres!");
		return in;
	}
	
	public String readLine(BufferedReader myReader) throws IOException {
		return myReader.readLine();
	}
	
	public static void readClose(BufferedReader myReader) throws IOException {
		myReader.close();
	} 

}
