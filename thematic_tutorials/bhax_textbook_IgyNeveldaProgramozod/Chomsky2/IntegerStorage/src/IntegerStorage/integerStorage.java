package IntegerStorage;

public class integerStorage {
	private int[] storage;
	private int index;
	
	public integerStorage(int index)
	{
		storage = new int[index];
		this.index=index;
	}
	
	public void add(int a)
	{
		if(index==0)
		{
			System.out.println("ERROR: Out of array!");
			return;
		}
		
		//az index jelöli a hátralévő elemek számát így max elemszám-index=következő üres elem
		storage[storage.length-index--] = a;
		return;
	}
	
	public boolean keres(int a)
	{
		
		//Ha üres a tömb
		if(storage.length==index)
		{
			System.out.println("ERROR: No element in this array!");
			return false;
		}
		
		//a bin keresés csak már előre rendezett soron működik
		this.rendez();
		
		//a bin keresés
		int e=0;
		int u= storage.length - index;
		
		while (e <= u )
		{
			int k = (e+u)/2;
			if (a < storage[k])
			{
				u = k - 1;
			}
			else if(a > storage[k])
			{
				e = k+1;
			}
			else return true;
		}
		return false;
	}
	
	public void rendez()
	{
		//Ha üres a tömb
		if(storage.length==index)
		{
			System.out.println("ERROR: No element in this array!");
			return;
		}
		
		//bubi rendezés
		for (int i=storage.length; i>0;i--)
		{
			for (int j=0; i<=i-1; j++)
			{
				if (storage[j] < storage[j+1])
					//csere
				{
					storage[j+1] += storage[j];
					storage[j] = storage[j+1] - storage[j];
					storage[j+1] = storage[j+1] - storage[j];
				}
			}
		}
		return;
	}
}
