#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <unistd.h>

int
main (void)
{
    int xj = 0, xk = 0, yj = 0, yk = 0;
    int mx = 80 * 2, my = 24 * 2;    //ez a x, és y mérete*2 a parancssornak

    WINDOW *ablak;
    ablak = initscr ();
    noecho ();
    cbreak ();
    nodelay (ablak, true);

    for (;;)
    {
        xj = (xj - 1) % mx;        //Ez adja az ismétlődését
        xk = (xk + 1) % mx;        //
                                //
        yj = (yj - 1) % my;        //
        yk = (yk + 1) % my;        //

        clear ();                //ne spamelje tele

        mvprintw (abs ((yj + (my - yk)) / 2),        //y koordinátája
                  abs ((xj + (mx - xk)) / 2), "X");    //x kordinátája
                  //osztom 2-vel h bennemaradjon a cmd-ben

        refresh ();
        usleep (150000);        //általunk is látható legyen

    }
    return 0;
}
//kódot fordítani gcc patt_if-nelkul.c -o out -lncurses
