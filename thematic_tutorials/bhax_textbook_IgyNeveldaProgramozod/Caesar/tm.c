#include <stdio.h>
#include <stdlib.h>

int
main ()
{
    int nr = 5;
    double **tm;
    
    //memória cím kiiratás

    printf("%p\n", &tm);
    
	//tm-ben foglalunk helyet nr számú "sornak" és megnézzük hogy van-e hiba

    if ((tm = (double **) malloc (nr * sizeof (double *))) == NULL)
    {
        return -1;
    }

	//memória cím kiiratás
    printf("%p\n", tm);
    
	//itt végigmegyünk a sorokon és az adott sorokban a sorszám szerint foglalunk helyet

    for (int i = 0; i < nr; ++i)
    {
        if ((tm[i] = (double *) malloc ((i + 1) * sizeof (double))) == NULL)
        {
            return -1;
        }

    }

	//memória cím kiiratás	

    printf("%p\n", tm[0]);    
    
	//első for ciklus sor, második a soron belüli értékeket viszi	

    for (int i = 0; i < nr; ++i)
        for (int j = 0; j < i + 1; ++j)
            tm[i][j] = i * (i + 1) / 2 + j; //értékadás

	//kiiratás

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }

	//hivatkozások

    tm[3][0] = 42.0;
    (*(tm + 3))[1] = 43.0;	// mi van, ha itt hiányzik a külső ()
    *(tm[3] + 2) = 44.0;
    *(*(tm + 3) + 3) = 45.0;

	//kiirás	

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }

	//felszabadítjuk a területet

    for (int i = 0; i < nr; ++i)
        free (tm[i]);

    free (tm);

    return 0;
}
