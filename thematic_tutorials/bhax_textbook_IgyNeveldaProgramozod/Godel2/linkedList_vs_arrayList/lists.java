import java.util.ArrayList;
import java.util.LinkedList;

public class lists {
    public static void main(String[] args) {
        ArrayList arraylist = new ArrayList();
        LinkedList linkedlist = new LinkedList();



// arraylist add
        long starttime = System.nanoTime();
        for (int i = 0; i < 100000; i++) {
            arraylist.add(i);
        }
            long endtime = System.nanoTime();
            long duration = endtime - starttime;
            System.out.println("arraylist add:  " + duration);



// linkedlist add
            starttime = System.nanoTime();
            for (int i = 0; i < 100000; i++) {
                linkedlist.add(i);
            }
            endtime = System.nanoTime();
            duration = endtime - starttime;
            System.out.println("linkedlist add: " + duration);



// arraylist get
            starttime = System.nanoTime();
            for (int i = 0; i < 10000; i++) {
                arraylist.get(i);
            }
            endtime = System.nanoTime();
            duration = endtime - starttime;
            System.out.println("arraylist get:  " + duration);



// linkedlist get
            starttime = System.nanoTime();
            for (int i = 0; i < 10000; i++) {
                linkedlist.get(i);
            }
            endtime = System.nanoTime();
            duration = endtime - starttime;
            System.out.println("linkedlist get: " + duration);



// arraylist remove
            starttime = System.nanoTime();
            for (int i = 0; i < 10000; i++) {
                arraylist.remove(i);
            }
            endtime = System.nanoTime();
            duration = endtime - starttime;
            System.out.println("arraylist remove:  " + duration);



// linkedlist remove
            starttime = System.nanoTime();
            for (int i = 0; i < 10000; i++) {
                linkedlist.remove(i);
            }
            endtime = System.nanoTime();
            duration = endtime - starttime;
            System.out.println("linkedlist remove: " + duration);
    }
}

